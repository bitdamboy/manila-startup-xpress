const express         = require('express');
const morgan          = require('morgan');
const helmet          = require('helmet');
const cors            = require('cors');
const path            = require('path');
const mongoose        = require('mongoose');
const passport        = require('passport');
const session         = require('express-session');
const methodOverride  = require('method-override');
const flash           = require('connect-flash');
const expressLayouts  = require('express-ejs-layouts');
const connectDB       = require('./app/config/db');

// Load dotenv
require('dotenv').config();

// Load DB Connection
connectDB();

// Initialize express
const app = express();

// Load Passport Config
require('./app/config/passport')(passport);

// Require Controller
const Index = require('./app/routes/index');
const User  = require('./app/routes/users');
const Dash  = require('./app/routes/dashboard');

// Load Middleware
const middlewares = require('./app/config/middlewares');

app.use(morgan(':method :url :status :res[content-length] - :response-time ms')); // Morgan
app.use(methodOverride('_method')); // Method Override
app.use(helmet()); // Helmet
app.use(cors()); // Cors
app.use(express.urlencoded({ extended: false }))
app.use(flash());

// Static files middleware
app.use(express.static(path.join(__dirname, 'public')));

// Set views path and default layout
app.set('views', path.join(__dirname, 'app/views'));

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Express Session
app.use(session({
  secret: 'this is secret shhhhhhhh',
  resave: false,
  saveUninitialized: true
}));

// Passport Midlleware
app.use(passport.initialize());
app.use(passport.session());

// Declare global lVariables
app.use((req, res, next) => {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	next();
});

// Routes
app.use('/', Index);
app.use('/user', User);
app.use('/dashboard', Dash);
app.get('*', function(req, res) {
  res.sendStatus(404);
});

// Handle Error
app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

// Port
const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`Server started on port ${PORT}`));