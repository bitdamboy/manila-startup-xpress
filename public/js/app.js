// $(document).ready(function() {
//   console.log('Ready');
// });

// Function
function jobOpening() {
	var jobOpening = new Swiper('.js__jobOpenning', {
      loop: true,
      // autoplay: true,
      // effect: 'slide',
      slidesPerView: 1,
      slidesPerGroup: 3,
      spaceBetween: 10,
      loopFillGroupWithBlank: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
         navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        breakpoints: {
        // when window width is <= 320px
        768: {
          slidesPerView: 3
        },
        425: {
          slidesPerView: 2
        },
        320: {
          slidesPerView: 1
        }
      }
  });
}


$(function() {
    jobOpening();
}); // ready

$(window).on('load', function() {}); // load