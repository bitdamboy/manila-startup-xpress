// Passport
const passport = require('passport');
const bcryptjs = require('bcryptjs');

// Require Model 
const User = require('../models/userModel');

// Begin Controller
exports.index = (req, res) => {
  res.send('NOT IMPLEMENTED: User home page');
};

// Display Registration Form
exports.user_registration = (req, res) => {
	res.render('./users/register');
};

// Handle Registration Form
exports.user_registration_post = (req, res) => {
    
	const { name, email, password, password2 } = req.body;
  
  let errors = [];

  //Check fields 
	if ( !name || !email || !password || !password2 ) {
		errors.push({ msg: 'Please fill in all fields!' });
	}

	//Check password match
	if (password !== password2) {
		errors.push({ msg: 'Passwords do not match'});
	}

	//Check Password Length
	if (password.length < 6 ) {
		errors.push({ msg: 'Password should be at least 6 characters'})
	}

  if (errors,length > 0) {
    errors.push({ msg: 'Password should at least 6 characters'})
  }

  if (errors.length > 0) {
    res.render('./users/register', {
      errors, 
      name, 
      email,
      password,
      password2
    })
  } else {
    // Validation Pass
    User.find({ email: email})
      .then( user => {
        if(user) {
          // user exists
          errors.push({ msg: 'Email is already registered!'});
          res.render('./user/register', {
            errors, 
            name, 
            email,
            password,
            password2
          });
        } else {
          const newUser = new User({
            name, 
            email,
            password
          });

          // Hash Password
          bcryptjs.genSalt(10, (err, salt) => {
            bcryptjs.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              //Set the password hash
              newUser.password = hash;
              //Then save user
              newUser.save()
                .then( user => {
                  req.flash('success_msg', 'You are now registered and can now log in');
                  res.redirect('/user/login');
                })
                .catch(err => console.log(err));
            })
          })
        }
      })
};

// Display Login Form
exports.user_login = (req, res) => {
  res.render('./users/login');
};

// Handle Login Form
exports.user_login_post = (req, res, next) => {
	console.log(req.body);
	
	passport.authenticate('local', {
    successRedirect: '/create',
    failureRedirect: '/user/login',
    failureFlash: true
  })(req, res, next);
};

// Display list of all users.
exports.user_list = (req, res) => {
  res.send('NOT IMPLEMENTED: Users list');
};

// Display detail page for a specific user
exports.user_detail = (req, res) => {
  res.send('NOT IMPLEMENTED: Users detail:' + req.params.id);
};

// Display user create from on GET
exports.create_user_get = (req, res) => {
  res.send('NOT IMPLEMENTED: Create user GET')
}

// Handle user create on POST
exports.create_user_post = (req, res) => {
  res.send('NOT IMPLEMENTED: Create user GET')
}

// Display user delete form on GET
exports.delete_user_get = (req, res) => {
  res.send('NOT IMPLEMENTED: user delete GET')
}

// Display user delete form on POST
exports.delete_user_post = (req, res) => {
  res.send('NOT IMPLEMENTED: user delete POST')
}

// Display user update form on GET
exports.update_user_get = (req, res) => {
  res.send('NOT IMPLEMENTED: user update GET')
}

// Display user update form on POST
exports.update_user_post = (req, res) => {
  res.send('NOT IMPLEMENTED: user update POST')
}