const express   = require('express');
const router    = express.Router();

// Begin Router
router.get('/', (req, res) => {
  res.render('./dashboard/index.ejs');
});

module.exports = router;