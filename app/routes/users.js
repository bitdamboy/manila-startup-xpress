const express = require('express');
const router = express.Router();

// Require Controller Modules
const User = require('../controller/userController');

// GET user home page
router.get('/', User.index);

// GET Login Page
router.get('/login', User.user_login);

// POST Login Page
router.post('/login', User.user_login_post);

// GET Show Registration Form
router.get('/register', User.user_registration);

// POST Show Registration Form
router.post('/register', User.user_registration_post);

// GET request for list of all users
router.get('/all', User.user_list);

// GET request for creating a User. Note this must come before routes that display user ID.
router.get('/create', User.create_user_get);

// POST request for creating User.
router.post('/create', User.create_user_post);

// GET request to delete User.
router.get('/:id/delete', User.delete_user_get);

// POST request to delete User.
router.post('/:id/delete', User.delete_user_post);

// GET request to update a User.
router.get('/:id/update', User.update_user_get);

// POST request to update a User.
router.post('/:id/update', User.update_user_post);

// GET request for one use
router.get('/:id', User.user_detail);

module.exports = router;